angular.module('mainApp', ['ngRoute', 'ui.bootstrap', 'angulike']).

constant( 'baseUrl', 'http://api.base.com/' ).


config(function ($routeProvider, $locationProvider, baseUrl, $httpProvider) {
    
    $locationProvider.html5Mode(true);
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $routeProvider
    .when('/', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html'
    }) 
    .when('/test', { 
        controller: 'testCtrl', 
        templateUrl: 'src/app/views/test.html' 
    })
    .otherwise({ redirectTo: '/' });
});
angular.module('mainApp')
.controller('homeCtrl', function ($scope, $location, $rootScope, baseUrl, $document) {
	$scope.language = "english";
	$scope.title_text = "Elevator Pitch Generator";
	$scope.change_button_text = "日本語で";
	$scope.pitch_button_text = "Pitch Me!";
	$scope.startupIdea = "";
	
	//
	$document.bind("keydown", function(event) {
        if($scope.language == "english"){
			$scope.createStartupIdea("english");	
			$scope.$apply();	
		}
		else if($scope.language == "japanese"){
			$scope.createStartupIdea("japanese");	
			$scope.$apply();
		}
    });
	
	$scope.changeLanguage = function(language){
		if(language == "japanese"){
			$scope.language = "english";
			$scope.title_text = "Elevator Pitch Generator";
			$scope.change_button_text = "日本語で";
			$scope.pitch_button_text = "Pitch Me!"
			$scope.startupIdea = createEnglishStartupIdea();
		}
		else if(language == "english"){
			$scope.language = "japanese";
			$scope.title_text = "エレベーターピッチジェネレータ";
			$scope.change_button_text = "In English";
			$scope.pitch_button_text = "ミーピッチ"
			$scope.startupIdea = createJapaneseStartupIdea();
		}
	}
	
	$scope.createStartupIdea = function(language){
		if(language == "english"){
			$scope.startupIdea = createEnglishStartupIdea();			
		}
		else if(language == "japanese"){
			$scope.startupIdea = createJapaneseStartupIdea();
		}
	}
	$scope.createStartupIdea($scope.language);
	
	function createEnglishStartupIdea(){
		var intro=["We are","Basically, we're","We're building"];
		var startup_adjective=["transparent","deep","semantic","agile","agile","agile","agile","real-time","open source","aggregate","scalable","accurate","customizable","premium","next generation","personalized","asynchronous","authentic","powerful","radical","impactful","innovative","disruptive","cutting-edge","lean"];
		var startup_market=["enterprise","biotech","machine learning","social","marketing automation","e-commerce","SaaS","mobile","B2B","B2B","big data analytics","consumer-based","data-driven","crowdsourced","publishing","crowdfunding","cloud-based","cloud-based","cloud-based","content-curation","mobile-first","genome analysis","native advertising","CMS"];
		var startup_noun=["platform","network","application","API","marketplace","toolbox","interface","ecosystem","loyalty platform"];
		var startup_verb=["disrupts","scales","connects","generates","expedites","mobilizes","revolutionizes","partners","transforms","synchronizes","maximize","shifts the paradigms of","leverages"];
		var market_noun=["web marketing","local search","sales funnels","angel investors","clients","venture capital firms","small businesses","content distribution","gamification","bitcoins","hotels","team collaboration","financial projections","biomedical research","social media","microblogging","ROT","metrics"];
		var connector=["through","with","and","using","along","into"];
		var market_adjective=["game-changing","transparent","deep","semantic","agile","real-time","open source","aggregate","scalable","accurate","customizable","premium","traditional","highly relevant","visual","next generation","personalized","asynchronous","authentic","powerful","radical","impactful","innovative","ground-breaking","state-of-the-art","streamlined","dynamic"];
		var outro=["non-profits","synergy","virality","group messaging","pet shops","video messaging","hospitality","fine-dining restaurants","tablets","online advertising","consumers","iteration cycles","interest graphs","3D motion sensing","commercial banking","mentorship","minimum viable products","engagement methods"];
		
		function random(array){
			return array[Math.floor(Math.random()*array.length)];
		}
		
		function startsWithVowel(word){
			var vowels = ['a','e','i','o','u'];
			if(!vowels.indexOf(word[0])){
				return " an ";
			}
			else{
				return " a ";
			}
		}
		
		var result = random(intro);
		var helper = random(startup_adjective);
		result += startsWithVowel(helper);
		result += helper + " ";
		result += random(startup_market) + " ";
		result += random(startup_noun);
		result += " that ";
		result += random(startup_verb) + " ";
		result += random(market_noun) + " ";
		result += random(connector)  + " ";
		result += random(market_adjective)  + " ";
		result += random(outro);
		
		return result;
	}
	
	function createJapaneseStartupIdea(){
		var intro=["私たちです","基本的に、私たちはしています","私たちは、構築しています"];
		var startup_adjective=["透明な","深いです","セマンティック","アジャイル","リアルタイム","オープンソース","集計","拡張性のあります","正確な","カスタマイズ可能な","プレミアム","次の世代","パーソナライズ","非同期","本物の","パワフル","ラジカル","インパクトの強いです","革新的な","破壊的な","最先端の","リーン"];
		var startup_market=["企業","バイオテクノロジー","機械学習","ソーシャル","マーケティングオートメーション","eコマース","SaaSの","モバイル","B2B","ビッグデータ分析","消費者ベース","データ駆動型の","クラウドソーシング","出版","クラウドファンディング","クラウドベース","コンテンツキュレーション","モバイル・ファースト","ゲノム分析","ネイティブ広告","CMS"];
		var startup_noun=["プラットフォーム","ネットワーク","アプリケーション","API","市場","ツールボックス","インターフェース","エコシステム","ロイヤルティ・プラットフォーム"];
		var startup_verb=["乱します","秤","コネクト","生成","迅速","動員","革命をもたらしました","パートナー","トランスフォーム","同期さ","最大化します","のパラダイムをシフト","レバレッジ"];
		var market_noun=["ウェブマーケティング","局所探索","販売ファンネル","エンジェル投資家","クライアント","ベンチャー投資会社","中小企業","コンテンツ配布","ゲーミフィケーション","ビットコイン","ホテル","チームコラボレーション","財務予測","生物医学研究","ソーシャルメディア","マイクロブロギング","ROT","メトリクス"];
		var connector=["経て","とともに","と","使用して","沿って","に"];
		var market_adjective=["試合の流れを変えます","トランスペアレント","深いです","セマンティック","アジャイル","リアルタイム","オープンソース","集計","拡張性のあります","正確な","カスタマイズ可能な","プレミアム","伝統的な","関連性の高いです","視覚の","次世代","パーソナライズ","非同期","本物の","パワフル","ラジカル","インパクトの強いです","革新的な","画期的な","最先端","合理化されました","ダイナミック"];
		var outro=["非営利団体","相乗効果","バイラリティー","グループメッセージ","ショップペット","ビデオメッセージング","ホスピタリティー","レストランを微ダイニング","タブレット","オンライン広告","消費者","反復サイクル","金利グラフ","3Dモーションセンシング","商業銀行","指導","最小の実行可能な製品","婚約方法"];
		
		function random(array){
			return array[Math.floor(Math.random()*array.length)];
		}
		
		var result = random(intro);
		result += random(startup_adjective);
		result += random(startup_market);
		result += random(startup_noun)+"その";
		result += random(startup_verb);
		result += random(market_noun);
		result += random(connector);
		result += random(market_adjective);
		result += random(outro);
		
		return result;
	}

})
angular.module('mainApp')
.controller('navCtrl', function ($scope, $location, $rootScope) {

})
angular.module('mainApp')
.directive('keylistener', function() {
  return {
    restrict: 'E',
    replace: true,
    scope: true,
    link:    function postLink(scope, iElement, iAttrs){
      jQuery(document).on('keypress', function(e){
         scope.$apply(scope.keyPressed(e));
       });
    }
  };
});